package com.Mihai;

import org.junit.Test;

import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.*;

@RunWith(Parameterized.class)
public class SortingTest {
    public Sorting sorting = new Sorting();
    private int[] array;
    private int[] result;

    public SortingTest(int[] array, int[] result) {
        this.array = array;
        this.result = result;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data(){
        return Arrays.asList(new Object[][]{
                {new int[]{5, 4, 6, -1, 0}, new int[]{-1,0, 4, 5, 6}},
                {new int[]{1, 2, 3, 4, 5}, new int[]{1, 2, 3, 4, 5}},
                {new int[]{9, 8, 7, 6, 5, 4, 3, 2, 1}, new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9}},
                {new int[]{0}, new int[]{0}}
        });
    }

    @Test
    public void SortTest() {
        assertArrayEquals(this.result, sorting.sort(this.array));
    }

    @Test (expected = IllegalArgumentException.class)
    public void NullTest() {
        sorting.sort(null);
        sorting.sort(new int[]{});
    }
}