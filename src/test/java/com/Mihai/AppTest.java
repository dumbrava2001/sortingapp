package com.Mihai;

import org.junit.Test;

import java.util.ArrayList;

import java.util.List;

import static org.junit.Assert.*;

public class AppTest extends App {
    private List<String> args = new ArrayList<>();
    App app = new App();

    @Test(expected = IllegalArgumentException.class)
    public void testZeroArguments() {
        List<String> str1 = new ArrayList<>();
        app.creatingArray(str1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testOneArgument() {
        List<String> str2 = new ArrayList<>();
        str2.add("20");
        app.creatingArray(str2);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testMoreThanTenArguments(){
        List<String> str3 = new ArrayList<>();
        for (int i = 0; i <= 11; i++) {
            str3.add("2");
        }
        app.creatingArray(str3);
    }
}