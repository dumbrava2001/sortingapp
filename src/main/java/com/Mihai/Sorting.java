package com.Mihai;


public class Sorting {
    public int[] sort(int[] array) throws IllegalArgumentException{
        if (array == null || array.length == 0) {
            throw new IllegalArgumentException();
        }
        int temp;
        for (int i = 0; i < array.length; i++) {
            for (int j = i + 1; j < array.length; j++) {
                if (array[i] > array[j]) {
                    temp = array[j];
                    array[j] = array[i];
                    array[i] = temp;
                }
            }
        }
        return array;
    }
}
