package com.Mihai;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class App
{
    public static int[] creatingArray(List<String> numbers) throws IllegalArgumentException{
        if (numbers.size() == 0 || numbers.size() == 1 || numbers.size() >= 10){
            throw new IllegalArgumentException("<----Invalid boundary(corner case)---->");
        }
        int [] array = new int[numbers.size()];
        try {
            for (int i=0;i<array.length;i++) {
                array[i] = Integer.parseInt(numbers.get(i));
            }
            return array;
        } catch (NumberFormatException error) {
            throw new NumberFormatException("<----Invalid type entered---->");
        }
    }

    public static void main( String[] args )
    {
        Scanner input = new Scanner(System.in);
        List<String> numbers = new ArrayList<>();
        System.out.println("Enter up to 10 integer elements per line:");
        String userInput = new String();

        //getting the input
        while (true) {
            userInput = input.nextLine();
            if (userInput.equals("")) {
                break;
            }
            numbers.add(userInput);
        }

        //Getting array from String
        int[] array = new int[0];
        try {
            array = creatingArray(numbers);
        } catch (Exception e) {
            e.printStackTrace();
        }

        //sorting numbers
        Sorting sorting = new Sorting();

        int[] result = sorting.sort(array);
        //printing
        System.out.println("Sorted numbers:");
        for (int elem :
                result) {
            System.out.printf("%d ", elem);
        }
    }
}
